import storage from 'node-persist'
import Character from '../model/Character'

import { getUser } from './User'

export const getAllCharacters = async (): Promise<Character[]> => {
  const characters = await storage.getItem('characters')
  return characters
}

export const getCharacter = async (id: string): Promise<Character> => {
  const characters = await getAllCharacters()
  const character = characters.find((item: Character) => String(item.id) === id)
  if (!character) {
    throw new Error('Not found')
  }
  return character
}

export const getUserCharacters = async (
  id: string,
  version: string = 'v1'
): Promise<Character[]> => {
  const user = await getUser(id, version)
  const characters = await getAllCharacters()

  const userCharacters = characters.filter((character: Character) =>
    user.characters.includes(character.id)
  )
  return userCharacters
}

export const setCharacter = async (character: Character) => {
  const characters = await getAllCharacters()

  // update character in characters
  const index = characters.findIndex(
    (item: Character) => String(item.id) === String(character.id)
  )

  if (index === -1) {
    characters.push(character)
  } else {
    characters[index] = { ...characters[index], ...character }
  }

  await storage.setItem('characters', characters)
}

export const updateCharacter = async (
  characterId: string,
  character: Character
) => {
  const oldCharacter = await getCharacter(characterId)

  // Prevent changing id and items
  character.id = oldCharacter.id
  character.items = oldCharacter.items

  setCharacter(character)
}
