import storage from 'node-persist'

import { db } from '../db'
import { UserV2 } from '../model/User'
import { v1tov2List } from '../util/adapter'

export const getUsers = async (version: string = 'v1'): Promise<UserV2[]> => {
  let users: UserV2[] = []

  // Factory

  if (version === 'v1') {
    users = v1tov2List(await storage.getItem('users'))
  } else if (version === 'v2') {
    users = db.get('users').value()
  }
  return users
}

export const getUser = async (
  id: string,
  version: string = 'v1'
): Promise<UserV2> => {
  const users = await getUsers(version)
  const user = users.find((item: UserV2) => item.id === id)

  if (!user) {
    throw new Error('Not found')
  }

  return user
}
