import storage from 'node-persist'
import Item from '../model/Item'

import { getCharacter, setCharacter } from './Character'

export const getItems = async (): Promise<Item[]> => {
  const items = await storage.getItem('items')
  return items
}

export const getItem = async (id: string): Promise<Item> => {
  const items = await getItems()
  const item = items.find((item: Item) => item.id === id)
  if (!item) {
    throw new Error('Not found')
  }
  return item
}

export const getCharacterItems = async (
  characterId: string
): Promise<string[]> => {
  const character = await getCharacter(characterId)
  return character.items
}

export const addItem = async (item: Item): Promise<Item> => {
  const items = await getItems()

  // item id = item_n
  const max = items.reduce((max: number, item: Item) => {
    const num = Number(item.id.split('_')[1])
    return num > max ? num : max
  }, 0)

  const newItem = {
    ...item,
  }
  newItem.id = `item_${max + 1}`

  items.push(newItem)
  await storage.setItem('items', items)
  return newItem
}

export const removeItem = async (id: string): Promise<void> => {
  const items = await getItems()
  const newItems = items.filter((item: Item) => item.id !== id)
  await storage.setItem('items', newItems)
}

export const removeCharacterItem = async (
  characterId: string,
  itemId: string
): Promise<void> => {
  const characterItems = await getCharacterItems(characterId)
  const newCharacterItems = characterItems.filter(
    (item: string) => item !== itemId
  )

  if (characterItems.length === newCharacterItems.length) {
    throw new Error('Not found')
  }

  const character = await getCharacter(characterId)
  character.items = newCharacterItems
  await setCharacter(character)
}
