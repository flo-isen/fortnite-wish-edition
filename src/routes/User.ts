import { Router } from 'express'

import * as Controller from '../controller/User'
import * as CharacterController from '../controller/Character'

const router = Router()

// ---------------------------------------------------------------------------

router.get('/', async (req, res) => Controller.getAllUsers(req, res))

router.get('/:id', async (req, res) => Controller.getUserById(req, res))

router.get('/:id/characters', async (req, res) =>
  CharacterController.getCharactersByUserId(req, res)
)

export default router
