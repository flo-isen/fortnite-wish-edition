import { Router } from 'express'

import * as Controller from '../controller/Character'
import * as ItemController from '../controller/Item'

const router = Router()

// ---------------------------------------------------------------------------

router.get('/', async (req, res) => Controller.getAllCharacters(req, res))

router.get('/:id', async (req, res) => Controller.getCharacterById(req, res))

router.get('/:id/items', async (req, res) =>
  ItemController.getItemsByCharacterId(req, res)
)

// ---------------------------------------------------------------------------

router.post('/:characterId/items/:itemId', async (req, res) =>
  ItemController.addItemToCharacter(req, res)
)

// ---------------------------------------------------------------------------

router.put('/:characterId', async (req, res) =>
  Controller.updateCharacter(req, res)
)

// ---------------------------------------------------------------------------

router.delete('/:characterId/items/:itemId', async (req, res) =>
  ItemController.removeItemFromCharacter(req, res)
)

export default router
