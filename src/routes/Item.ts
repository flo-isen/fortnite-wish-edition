import { Router } from 'express'

import * as Controller from '../controller/Item'

const router = Router()

// ---------------------------------------------------------------------------

router.get('/', async (req, res) => Controller.getAllItems(req, res))

router.get('/:id', async (req, res) => Controller.getItem(req, res))

// ---------------------------------------------------------------------------

router.post('/', async (req, res) => Controller.createItem(req, res))

// ---------------------------------------------------------------------------

router.delete('/:id', async (req, res) => Controller.deleteItem(req, res))

export default router
