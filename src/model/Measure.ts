export default class Measure {
  unit: string
  value: number

  constructor(unit: string, value: number) {
    this.unit = unit
    this.value = value
  }
}
