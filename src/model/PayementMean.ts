import PayementType from './PayementType'

export default class PayementMean {
  private id: string
  private type: PayementType
  private information: Record<string, unknown>

  constructor(id: string, type: PayementType) {
    this.id = id
    this.type = type
    this.information = {}
  }

  public getId(): string {
    return this.id
  }

  public getType(): PayementType {
    return this.type
  }

  public setType(type: PayementType): void {
    this.type = type
  }

  public getInformation(): Record<string, unknown> {
    return this.information
  }

  public setInformation(information: Record<string, unknown>): void {
    this.information = information
  }
}
