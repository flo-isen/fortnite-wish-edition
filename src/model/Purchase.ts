export default class Purchase {
  private id: string
  private amount: number
  private date: Date

  constructor(id: string, amount: number = 0, date: Date = new Date()) {
    this.id = id
    this.amount = amount
    this.date = date
  }

  public getId(): string {
    return this.id
  }

  public setId(id: string): void {
    this.id = id
  }

  public getAmount(): number {
    return this.amount
  }

  public getDate(): Date {
    return this.date
  }

  public setAmount(amount: number): void {
    this.amount = amount
  }

  public setDate(date: Date): void {
    this.date = date
  }
}
