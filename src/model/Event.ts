import Card from './Card'

export default class Event extends Card {
  #id: string

  constructor(
    id: string,
    url: string,
    title: string = '',
    description: string = '',
    image: string = ''
  ) {
    super(url, title, description, image)
    this.#id = id
  }

  get id(): string {
    return this.#id
  }

  set id(id: string) {
    this.#id = id
  }

  register = (userId: string): boolean => {
    console.log(`${userId} registered to ${this.#id}`)
    return true
  }
}
