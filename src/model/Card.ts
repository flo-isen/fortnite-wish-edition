export default class Card {
  url: string
  title: string
  description: string
  image: string

  constructor(
    url: string,
    title: string = '',
    description: string = '',
    image: string = ''
  ) {
    this.url = url
    this.title = title
    this.description = description
    this.image = image
  }
}
