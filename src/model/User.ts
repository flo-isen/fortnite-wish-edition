import PayementMean from './PayementMean'
import Purchase from './Purchase'

export default class User {
  private wallet: PayementMean[]
  private purchases: Purchase[]

  constructor(wallet: PayementMean[] = [], purchases: Purchase[] = []) {
    this.wallet = wallet
    this.purchases = purchases
  }

  public getWallet(): PayementMean[] {
    return this.wallet
  }

  public setWallet(wallet: PayementMean[]): void {
    this.wallet = wallet
  }

  public getPurchases(): Purchase[] {
    return this.purchases
  }

  public setPurchases(purchases: Purchase[]): void {
    this.purchases = purchases
  }
}

export class UserV1 {
  id: number
  name: string
  selectedCharacter: string
  characters: string[]

  constructor(
    id: number,
    name: string,
    selectedCharacter: string,
    characters: string[] = []
  ) {
    this.id = id
    this.name = name
    this.selectedCharacter = selectedCharacter
    this.characters = characters
  }
}

export class UserV2 {
  id: string
  old_id: number
  username: string
  playedCharacter: number
  characters: number[]

  constructor(
    id: string,
    old_id: number,
    username: string,
    playedCharacter: number,
    characters: number[] = []
  ) {
    this.id = id
    this.old_id = old_id
    this.username = username
    this.playedCharacter = playedCharacter
    this.characters = characters
  }
}
