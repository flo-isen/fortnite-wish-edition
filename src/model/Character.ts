import Coordinates from './Coordinates'
import Measure from './Measure'
import Entity from './Entity'

export default class Character extends Entity {
  name: string
  goldAmount: number

  constructor(
    id: number,
    name: string,
    health: number = 100,
    manaLevel: number = 0,
    forceLevel: number = 0,
    coordinates: Coordinates = { x: 0, y: 0, z: 0 },
    items: string[] = [],
    weight: Measure = { value: 0, unit: 'kg' },
    height: Measure = { value: 0, unit: 'm' },
    goldAmount: number = 0
  ) {
    super(id, health, manaLevel, forceLevel, coordinates, items, weight, height)
    this.name = name
    this.goldAmount = goldAmount
  }
}
