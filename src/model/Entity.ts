import Coordinates from './Coordinates'
import Measure from './Measure'

export default class Entity {
  id: number
  health: number
  manaLevel: number
  forceLevel: number
  coordinates: Coordinates
  items: string[]
  weight: Measure
  height: Measure

  constructor(
    id: number,
    health: number = 100,
    manaLevel: number = 0,
    forceLevel: number = 0,
    coordinates: Coordinates = { x: 0, y: 0, z: 0 },
    items: string[] = [],
    weight: Measure = { value: 0, unit: 'kg' },
    height: Measure = { value: 0, unit: 'm' }
  ) {
    this.id = id
    this.health = health
    this.manaLevel = manaLevel
    this.forceLevel = forceLevel
    this.coordinates = coordinates
    this.items = items
    this.weight = weight
    this.height = height
  }
}
