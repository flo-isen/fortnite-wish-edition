import Coordinates from './Coordinates'
import Entity from './Entity'
import Measure from './Measure'

export default class Mob extends Entity {
  type: string

  constructor(
    id: number,
    type: string,
    health: number = 100,
    manaLevel: number = 0,
    forceLevel: number = 0,
    coordinates: Coordinates = { x: 0, y: 0, z: 0 },
    items: string[] = [],
    weight: Measure = { value: 0, unit: 'kg' },
    height: Measure = { value: 0, unit: 'm' }
  ) {
    super(id, health, manaLevel, forceLevel, coordinates, items, weight, height)
    this.type = type
  }
}
