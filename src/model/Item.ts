import Measure from './Measure'

export default class Item {
  id: string
  name: string
  price: number
  type: string
  attributes: Measure[]

  constructor(
    id: string,
    name: string,
    price: number = 0,
    type: string = '',
    attributes: Measure[] = []
  ) {
    this.id = id
    this.name = name
    this.price = price
    this.type = type
    this.attributes = attributes
  }
}
