import { Router } from 'express'
import UserRouter from './routes/User'
import CharacterRouter from './routes/Character'
import ItemRouter from './routes/Item'
import { versionChecker } from './middleware/version'

export function createRouter() {
  const router = Router()

  const app = Router()
  router.use('/:version', versionChecker, app)

  app.use('/users', UserRouter)
  app.use('/characters', CharacterRouter)
  app.use('/items', ItemRouter)

  return router
}
