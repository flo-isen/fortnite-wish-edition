import stormdb from 'stormdb'
import * as fs from 'fs'

if (!fs.existsSync('./data/db')) {
  fs.mkdirSync('./data/db', { recursive: true })
}

const engine = new stormdb.localFileEngine('./data/db/db.stormdb')
export const db = new stormdb(engine)
