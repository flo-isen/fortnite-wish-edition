import { Request, Response } from 'express'
import * as Service from '../service/Item'
import * as CharacterService from '../service/Character'
import * as ItemService from '../service/Item'

export const getAllItems = async (_req: Request, res: Response) => {
  const items = await Service.getItems()
  res.json({ items })
}

export const getItem = async (req: Request, res: Response) => {
  const { id } = req.params
  try {
    const item = await Service.getItem(id)
    res.json({ item })
  } catch (error) {
    res.sendStatus(404)
  }
}

export const getItemsByCharacterId = async (req: Request, res: Response) => {
  try {
    const { id } = req.params
    const characterItems = await Service.getCharacterItems(id)
    res.json({ items: characterItems })
  } catch (err) {
    res.sendStatus(404)
  }
}

export const createItem = async (req: Request, res: Response) => {
  const item = req.body
  const newItem = await Service.addItem(item)
  res.json({ item: newItem })
}

export const addItemToCharacter = async (req: Request, res: Response) => {
  try {
    const { characterId, itemId } = req.params

    const characterItems = await ItemService.getCharacterItems(characterId)
    const item = await Service.getItem(itemId)
    characterItems.push(item.id)

    const character = await CharacterService.getCharacter(characterId)
    character.items = characterItems
    await CharacterService.setCharacter(character)
    res.sendStatus(204)
  } catch (err) {
    res.sendStatus(404)
  }
}

export const deleteItem = async (req: Request, res: Response) => {
  const { id } = req.params
  await Service.removeItem(id)
  res.sendStatus(204)
}

export const removeItemFromCharacter = async (req: Request, res: Response) => {
  try {
    const { characterId, itemId } = req.params
    await Service.removeCharacterItem(characterId, itemId)
    res.sendStatus(204)
  } catch (err) {
    res.sendStatus(404)
  }
}
