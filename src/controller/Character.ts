import { Request, Response } from 'express'
import * as Service from '../service/Character'

export const getAllCharacters = async (_req: Request, res: Response) => {
  const characters = await Service.getAllCharacters()
  res.json(characters)
}

export const getCharacterById = async (req: Request, res: Response) => {
  const { id } = req.params
  try {
    const character = await Service.getCharacter(id)
    res.json({ character })
  } catch (err) {
    res.sendStatus(404)
  }
}

export const getCharactersByUserId = async (req: Request, res: Response) => {
  try {
    const { id } = req.params
    const { version } = res.locals
    const userCharacters = await Service.getUserCharacters(id, version)
    res.json({ characters: userCharacters })
  } catch (err) {
    console.log(err)
    res.sendStatus(404)
  }
}

export const updateCharacter = async (req: Request, res: Response) => {
  try {
    const { characterId } = req.params
    const character = req.body
    character.id = Number.parseInt(characterId)
    await Service.updateCharacter(characterId, character)
    res.sendStatus(204)
  } catch (err) {
    res.sendStatus(404)
  }
}
