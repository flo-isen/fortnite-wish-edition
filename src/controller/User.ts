import { Request, Response } from 'express'

import * as Service from '../service/User'

export const getAllUsers = async (_req: Request, res: Response) => {
  const { version } = res.locals
  const users = await Service.getUsers(version)
  res.json({ users })
}

export const getUserById = async (req: Request, res: Response) => {
  try {
    const { id } = req.params
    const { version } = res.locals
    const user = await Service.getUser(id, version)
    res.json({ user })
  } catch (err) {
    res.sendStatus(404)
  }
}
