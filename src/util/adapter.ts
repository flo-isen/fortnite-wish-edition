import { UserV1, UserV2 } from '../model/User'

export const v1tov2 = (v1: UserV1): UserV2 => {
  return {
    id: v1.id.toString(),
    old_id: v1.id,
    username: v1.name,
    playedCharacter: Number(v1.selectedCharacter),
    characters: v1.characters.map((c) => Number(c)),
  }
}

export const v1tov2List = (v1: UserV1[]): UserV2[] => {
  return v1.map((u) => v1tov2(u))
}

export const v2tov1 = (v2: UserV2): UserV1 => {
  return {
    id: v2.old_id,
    name: v2.username,
    selectedCharacter: v2.playedCharacter.toString(),
    characters: v2.characters.map((c) => c.toString()),
  }
}

export const v2tov1List = (v2: UserV2[]): UserV1[] => {
  return v2.map((u) => v2tov1(u))
}
