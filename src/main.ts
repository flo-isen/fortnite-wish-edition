import { HttpServer } from './Server'
import { App } from './App'

import { createRouter } from './router'
import { initStorage } from './storage'

// Inject data to local storage
import './data'

const server = new HttpServer(8000, new App(createRouter()).instance)
server.start()
initStorage()
